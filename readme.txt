Simple plugin to show/hide block 

Example
<code>
	<div class="form-group col-md-3">
		<label for="attend">Attending Camp</label>
		<select name="child[][attend]" class="form-control inline-input" id="attend" required>
			<option value="1" id="attend_full">Whole summer</option>
			<option value="2" id="attend_first">First half</option>
			<option value="3">Second half</option>
		</select>
	</div>
	<div class="form-group col-md-5">
		<label for="bunk">Bunk</label>
		<select name="child[][bunk]" class="form-control inline-input" id="bunk" required>
			<option value="1">Aleph Junior/Going in to Nursery</option>
			<option value="2">Aleph/Going in to Kindergarten</option>
			<option value="3">Bais/Going in to Primary</option>
			<option value="4" id="bunk-gimmel">Gimmel/Going in to First Grade</option>
			<option value="5" id="bunk-daled">Daled/Going in to Second Grade</option>
			<option value="6" id="bunk-hay">Hay/Going in to Third Grade</option>
		</select>
	</div>
	<div data-depend-on="(#bunk-gimmel|#bunk-daled|#bunk-hay)&(#attend_full|#attend_first)">
	</div>	 

	<div class="container-fluid">
		<div class="checkbox checkbox-primary">
			<input name="summer_as_home" id="summer-as-home" type="checkbox" value="1">
			<label for="summer-as-home">Same as Home Address</label>
		</div>
	</div>
	<div class="container-fluid" data-depend-on="!(#summer-as-home)">
	</div>
<code>