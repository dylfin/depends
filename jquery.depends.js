/**
 * jQuery Plugin to show/hide a section witch depends from other section
 * @version 1.02
 * @author Michael Valushko <michael.valushko@gmail.com>
 * @link https://bitbucket.org/dylfin/depends
 */


(function ( $ ) {
    "use strict";

    $.fn.depends = function(options) {

        var Processor = function(){

            /**
             * Elements IDs
             * @type {Array}
             */
            this.ids = [];
            this.conditions = '';

            this.search = function(items) {
                this.conditions = items;
                items = items.replace('!','');
                var re = /[()|&]?([^()|&]+)[()|&]?/g;
                var result;
                while((result = re.exec(items))!==null){
                    this.ids.push({
                        param: result[1],
                        value: false
                    });
                }
            };

            this.check = function(){
                var condition_string = this.conditions;
                $(this.ids).each(function(idx, item){
                    condition_string = condition_string.replace(item.param, item.value);
                });
                //console.log(condition_string);
                return eval(condition_string);
            }
        };

        var settings = $.extend({
            selector: "[data-depend-on]"
        }, options);

        var main = function(idx, scope){
            var $scope = $(scope);
            var event_id = 1;

            var items = $scope.find(settings.selector);
            items.each(function(i, el){
                var $el = $(el);

                var processor = new Processor();
                processor.search($el.data('depend-on'));

                var callback = function(){
                    if(processor.check()) {
                        $el.find('select,input,textarea').attr('disabled', false);
                        $el.show();
                    }
                    else {
                        $el.hide();
                        if($el.prop('nodeName')==="OPTION" && $el.is(':checked')){
                            var $select = $el.parent();
                            $select.find('option').each(function(i, el){
                                var $el = $(el);
                                $el.prop('selected', true);
                                if($el.css("display")!=='none') return false;
                            });
                        }
                        $el.find('select,input,textarea').attr('disabled', true);
                    }
                };

                $(processor.ids).each(function(j, e){
                    var $target = $scope.find(e.param);
                    if(!$target.length && scope.nodeName!=='#document'){
                        $target = $(document).find(e.param);
                    }
                    if($target.length){
                        var $select = null;
                        if($target.prop('nodeName')==="OPTION"){
                            $select = $target.parent();
                            console.log('depend-applied-'+event_id);
                            if(!$select.data('depend-applied-'+event_id)){
                                var _event_id = event_id;
                                $select.change(function(){
                                    var ids = $select.data('depend-applied-'+_event_id);
                                    $(processor.ids).each(function(i, v){
                                        if(ids.indexOf(i)===-1) return true;
                                        v.value = v.$target.is(':checked');
                                    });
                                    // e.value = e.$target.is(':checked');
                                    callback();
                                });
                                $select.data('depend-applied-'+event_id, [j]);
                            }
                            else{
                                var ids = $select.data('depend-applied-'+event_id);
                                ids.push(j);
                                $select.data('depend-applied-'+event_id, ids);
                            }
                            e.value = $target.is(':checked');
                            e.$target = $target;
                        }
                        if($target.prop('nodeName')==="INPUT" && $target.prop('type')==="checkbox"){
                            $select = $target;
                            $select.change(function(){
                                e.value = $target.is(':checked');
                                callback();
                            });
                            e.value = $target.is(':checked');
                        }
                        if($target.prop('nodeName')==="INPUT" && $target.prop('type')==="radio"){
                            var $buttons = $scope.find('input[name="' + $target.prop('name') + '"]');
                            $select = $target;
                            $buttons.change(function(){
                                e.value = (this.value === $select.val());
                                callback();
                            });
                            e.value = $target.is(':checked');
                        }
                        if($target.prop('nodeName')==="INPUT" && $target.prop('type')==="text"){
                            $select = $target;
                            $select.on('keyup', function(){
                                e.value = ($target.val().trim()!=='' && $target.val().trim()!=='0');
                                callback();
                            });
                            $select.on('change', function(){
                                e.value = ($target.val().trim()!=='' && $target.val().trim()!=='0');
                                callback();
                            });
                            e.value = ($target.val().trim()!=='' && $target.val().trim()!=='0');
                        }

                        if($target.prop('nodeName')==="INPUT" && $target.prop('type')==="hidden"){
                            $select = $target;
                            $select.on('change', function(){
                                e.value = ($target.val().trim()!=='' && $target.val().trim()!=='0');
                                callback();
                            });
                            e.value = ($target.val().trim()!=='' && $target.val().trim()!=='0');
                        }

                    }
                });
                callback();
                event_id ++;
            });
        };

        if(this.length) this.each(main);
        else main(0, document);


        return this;
    };

}( jQuery ));